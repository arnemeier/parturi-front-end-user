async function login(credentials) {
    let response = await fetch(
        `${SALON_API_URL}/user/login`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    return await handleJsonResponse(response);
}




async function postNewUser(newUser) {
    let response = await fetch(
        `${SALON_API_URL}/user/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(newUser)
        }
    );
    return await handleJsonResponse(response);
}


// V6tab vaikimisi teenused, mille abil teeb enda teenuse listi
async function fetchDefaultServices() {
    let response = await fetch(`${SALON_API_URL}/service/default`,)
    return await handleJsonResponse(response);
}

// LISA UUS TEENUS
async function postNewUserService(userService) {
    let response = await fetch(
        `${SALON_API_URL}/service/add`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(userService)
        }
    );
    return await handleJsonResponse(response);
}



// tõmba konkreetse töötaja broneeringud
async function fetchSelectedWorkerBookings(workerId) {
    let response = await fetch(
        `${SALON_API_URL}/booking/week/${workerId}`,
        {
            method: 'GET'
            // headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

// postita konkreetse juuksuri schedule'id
async function postWorkingSchedules(schedules) {
    // console.log(JSON.stringify(newUser));
    let response = await fetch(
        `${SALON_API_URL}/schedule/add`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(schedules)
        }
    );
    // console.log(response);
    return await handleJsonResponse(response);
}
