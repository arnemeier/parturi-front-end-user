/* 
    --------------------------------------------
    INIT
    --------------------------------------------
*/
let calendar;
let calendarEvents = [];

window.addEventListener('DOMContentLoaded', () => {
    const mainContainer = document.querySelector("#mainContainer");
    reload();
    loadHeader();
    if (getToken()) {
        updateHeader();
        // loadServiceChooser();
        // loadCalendarChooser();
        // loadCurrentUserOffersContainer();
        // loadCurrentUserOffers();
    }
    // loadOffers();


});


/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function loadHeader() {
    let navHeader = document.querySelector('#navBar').content;
    document.body.append(navHeader.cloneNode(true));
}

function updateHeader() {
    let navHeaderUsername = document.querySelector('header h6');
    let navHeaderRegisterLink = document.querySelector('#pleaseRegister');
    let navHeaderLoginToggle = document.querySelector('#loginToggle');
    if (!getToken()) {
        navHeaderUsername.innerHTML = '';
        navHeaderLoginToggle.innerHTML = 'Logi sisse';
        navHeaderLoginToggle.href = 'javascript:loadLoginForm()';
        navHeaderRegisterLink.innerHTML = 'Registreeri';
    } else {
        navHeaderUsername.innerHTML = /*html*/`
        <a href="javascript:fillUserData()">${getUsername()}</a>
        `;
        navHeaderRegisterLink.innerHTML = '';
        navHeaderLoginToggle.innerHTML = 'Logi välja';
        navHeaderLoginToggle.href = 'javascript:doLogout()';
    }
}

function reload() {
    // clearServices();
    mainContainer.innerHTML = '';
    loadChooserRow();
    loadServiceChooser();
    loadCalendarChooser();

}

function loadChooserRow() {
    let chooserRow = document.querySelector('#chooserRowTemplate').content;
    mainContainer.appendChild(chooserRow.cloneNode(true));
}

function clearChooserRow() {
    mainContainer.querySelector('#chooserRow').remove();
}


// KUVA JUUKSURILE VAIKIMISI TEENUSED, MILLE ABIL TEHA TEENUSTE LIST
function loadServiceChooser() {
    let serviceChooser = document.querySelector('#serviceChooserTemplate').content;
    mainContainer.querySelector('#chooserRow').appendChild(serviceChooser.cloneNode(true));
}

function clearServiceChooser() {
    document.querySelector('#serviceChooser').remove();
}

function loadCalendarChooser() {
    let loadCalendarChooser = document.querySelector('#calendarChooserTemplate').content;
    mainContainer.querySelector('#chooserRow').appendChild(loadCalendarChooser.cloneNode(true));
}

function clearCalendarChooser() {
    document.querySelector('#calendarChooser').remove();
}

// siin peaks olema ka id sisend, et saaks anda tagasiside teemal, et kes täpsemalt midagi muutis 
async function editServices() {
    clearChooserRow();
    let services = document.querySelector('#servicesTemplate').content;
    mainContainer.appendChild(services.cloneNode(true));
    const defaultServices = await fetchDefaultServices();
    let servicesForm = document.querySelector('#servicesForm div');
    let iterator = 0;


    defaultServices.forEach(service => {
        iterator++;
        servicesForm.innerHTML += `
        <div class="form-group row">
            <div class="col-md-4">
                <div class="form-check mt-4">
                    <input class="form-check-input" type="checkbox" id="gridCheck${iterator}" onclick=enableRow(${iterator})>
                    <label class="form-check-label" for="gridCheck1">${service}</label>
                </div>
            </div>
            <div class="col-md-4">
                <input type="number" class="form-control mt-3" id="price${iterator}" placeholder="Sisesta hind" disabled>
            </div>
            <div class="col-md-4">
                <input type="number" class="form-control mt-3" id="duration${iterator}" placeholder="Sisesta kestvus" disabled>
            </div>
        </div>`
    });
}

function enableRow(iterator) {
    const priceRow = document.querySelector(`#price${iterator}`);
    const durationRow = document.querySelector(`#duration${iterator}`);

    if (priceRow.hasAttribute('disabled')) {
        priceRow.removeAttribute('disabled');
        durationRow.removeAttribute('disabled');
    } else {
        priceRow.setAttribute('disabled', true);
        durationRow.setAttribute('disabled', true);
    }
}


// JUUKSUR TEEB ENDALE TEENUSTE LISTI
async function createUserServiceList() {
    const currentUser = localStorage.getItem(AUTH_USERNAME);

    let servicesList = [];
    document.querySelectorAll('.form-group').forEach(node => {
        if (node.querySelector('input').checked) {
            let inputValues = [];
            node.querySelectorAll('.form-control').forEach(n => {
                inputValues.push(n.value);
            });

            servicesList.push({
                email: currentUser,
                service: node.querySelector('.form-check-label').innerHTML,
                price: inputValues[0],
                duration: inputValues[1]
            });
        }
    });
    servicesList.forEach(async (service) => {
        await postNewUserService(service);
    });
    reload();
    renderSuccessAlert("Teenuste lisamine õnnestus!");
    

}




function clearServices() {
    if (document.querySelector('#services')) {
        document.querySelector('#services').remove();
    }
}



// KALENDRIGA SEOTUD FUNKTSIOONID

async function loadCalendar() {
    let loadedCalendar = document.querySelector('#calendarTemplate').content;
    mainContainer.prepend(loadedCalendar.cloneNode(true));

    let draggableTemplate = document.querySelector('#draggableEventsTemplate').content;
    document.querySelector('#calendarContainer').prepend(draggableTemplate.cloneNode(true));

    let calendarElement = document.querySelector('#calendar');


    calendar = new FullCalendar.Calendar(calendarElement, {
        plugins: ['timeGrid', 'interaction'],
        // timeZone: 'UTC',
        defaultView: 'timeGridFourDay',
        selectable: true,
        selectConstraint: {
            startTime: '00:01',
            endTime: '23:59',
        },
        editable: true,
        droppable: true,
        header: {
            left: 'title',
            //   center: 'title',
            right: 'today,prev,next'
        },
        views: {

            timeGridFourDay: {
                allDaySlot: false,
                duration: { days: 7 },
                type: 'timeGrid',
                // buttonText: '4 day'
            }
        },
        locale: 'et',
        slotDuration: '00:30:00',
        // select: function (info) {
        //     console.log(info.end);
        //     addCalendarEvent(info.start, info.end);
        // },

        select: function (info) {
            calendar.addEvent({
                // title: 'dynamic event',
                start: info.start,
                end: info.end,
                title: 'work',
                color: '#b2dfdb'

                // allDay: true
            });
            let buttons = document.querySelectorAll('#cancelBookingsGroup > button');
            buttons.forEach(button => {
                button.disabled = false;
            });
        }


    });


    let clientEvents = await addClientEvents(2);

    clientEvents.forEach(clEvent => {
        calendar.addEvent(clEvent);
    });

    calendar.render();

    cancelSelectedSchedules();
    acceptSelectedSchedules();

}

function acceptSelectedSchedules() {
    let acceptedEventsArray = [];
    let acceptButton = document.querySelector('#confirmSchedulesButton');
    acceptButton.addEventListener('click', async () => {
        let acceptedEvents = calendar.getEvents();
        acceptedEvents.forEach(element => {
            if (element.title == 'work') {
                acceptedEventsArray.push({
                    schedule_manager: 1,
                    start_at: moment(element.start).format("YYYY-MM-DD HH:mm:ss"),
                    end_at: moment(element.end).format("YYYY-MM-DD HH:mm:ss")
                });
            }
        });
        console.log('palun, evendid on siin');
        console.log(acceptedEventsArray);

        let response = await postWorkingSchedules(acceptedEventsArray);
        // console.log();
        if (response.status == 200) {
            reload();
        } else {
            console.log('probleemid');
        }
    });
}

function cancelSelectedSchedules() {
    let cancelButton = document.querySelector('#cancelSchedulesButton');
    cancelButton.addEventListener('click', event => {
        calendar.getEvents().forEach(calEvent => {
            if (calEvent.title == 'work') {
                calEvent.remove();
            }
        });
        document.querySelectorAll('#cancelBookingsGroup > button').forEach(button => {
            button.disabled = true;
        });
    });
    calendar.render();
}

function editCalendar() {
    loadCalendar();
    clearChooserRow();
}


async function addClientEvents(hairdresser) {
    let clientEvents = [];
    let fetchedEvents = await fetchSelectedWorkerBookings(hairdresser);
    fetchedEvents.forEach(bookedEvent => {
        clientEvents.push({
            start: bookedEvent.start_at,
            end: bookedEvent.end_at,

            // rendering: 'background',
            // overlap: false,
            editable: false
        });
    });
    return clientEvents;
}




// REGISTREERIMISVORM
function loadRegisterForm() {
    if (!document.querySelector('#form-register')) {
        if (document.querySelector('#form-signin')) {
            clearLoginForm();
        }
        let registerFormTemplate = document.querySelector('#register').content;
        document.body.prepend(registerFormTemplate.cloneNode(true));
    }
}

function closeCloseForm() {
    document.querySelector('.form-signin').remove();
}




// CREATE NEW USER
async function createNewuser() {
    if (document.querySelector('.alert')) {
        closeAllAlerts();
    }
    try {
        let newUser = {
            first_name: document.querySelector('#inputFirstName').value,
            last_name: document.querySelector('#inputLastName').value,
            email: document.querySelector('#inputEmail').value,
            password: document.querySelector('#inputPassword').value,
            passwordRetyped: document.querySelector('#inputRepeatPassword').value,
            phone: document.querySelector('#inputPhone').value,
        };
        console.log(newUser);
        let validationResponse = validateRegistration(newUser);

        if (validationResponse.length == 0) {
            let response = await postNewUser(newUser);
            if (response.errors.length == 0) {
                renderSuccess("Registreerimine õnnestus!");
                setTimeout(function () {
                    alertClose();
                    loadLoginForm();
                }, 2500);
            } else {
                renderAlert(response.errors[0]);
            }
        } else {
            validationResponse.forEach(error => {
                renderAlert(error);
            });
        }

    } catch (error) {
        handleError(error, function () {
            loadHeader();
        });
    }
}

function clearRegisterForm() {
    document.querySelector('#form-register').remove();
}


function loadLoginForm() {
    if (!document.querySelector('#form-signin')) {

        if (document.querySelector('#form-register')) {
            clearRegisterForm();
        }
        let loginFormTemplate = document.querySelector('#signin').content;
        document.body.prepend(loginFormTemplate.cloneNode(true));
    }
}

function clearLoginForm() {
    document.querySelector('#form-signin').remove();
}




// LOGIN

async function doLogin() {
    // loadLoginForm();
    try {
        const credentials = {
            email: document.querySelector('#inputEmail').value,
            password: document.querySelector('#inputPassword').value,
        };
        const session = await login(credentials);
        storeAuthentication(session);
        clearLoginForm();
        updateHeader();
    } catch (e) {
        handleError(e, renderLoginAlert('Vale kasutaja või salasõna'));
    }
}

function doLogout() {
    reload();
    clearAuthentication();
    updateHeader();
}







