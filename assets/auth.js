const AUTH_TOKEN = "AUTH_TOKEN";
const AUTH_USERNAME = "AUTH_USERNAME";

function clearAuthentication() {
    localStorage.removeItem(AUTH_TOKEN);
    localStorage.removeItem(AUTH_USERNAME);
}

function storeAuthentication(session) {
    localStorage.setItem(AUTH_TOKEN, session.token);
    localStorage.setItem(AUTH_USERNAME, session.email);
}

function getUsername() {
    return localStorage.getItem(AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(AUTH_TOKEN);
}