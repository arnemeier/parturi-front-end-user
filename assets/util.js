function handleResponse(response) {
    if (response.status >= 401) {
        clearAuthentication();
        throw new Error('Unauthorized');
    }
}

async function handleJsonResponse(response) {
    if (response.status >= 401) {
        // clearAuthentication();
        throw new Error('Unauthorized');
    }
    return await response.json();
}

function handleError(e, errorCallback) {
    console.log('Error occurred:', e);
    if (e.message == 'Unauthorized') {
        errorCallback();
    }
}

function getTimeFromMins(mins) {
    // do not include the first validation check if you want, for example,
    // getTimeFromMins(1530) to equal getTimeFromMins(90) (i.e. mins rollover)
    if (mins >= 24 * 60 || mins < 0) {
        throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
    }
    var h = mins / 60 | 0,
        m = mins % 60 | 0;
    return moment.utc().hours(h).minutes(m).format("hh:mm");
}

/* 
    --------------------------------------------
    VALIDATION FUNCTIONS
    --------------------------------------------
*/

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateRegistration(item) {
    let errors = [];

    if (item.first_name.length < 2) {
        errors.push('Ebakorrektne eesnimi (nõutud vähemalt kaks tähemärki)!');
    }
    if (item.last_name.length < 2) {
        errors.push('Ebakorrektne perenimi (nõutud vähemalt kaks tähemärki)!');
    }

    if (item.password.length < 6 || item.passwordRetyped.length < 6) {
        errors.push('Salasõna minimaalne pikkus on 6 tähemärki!');
    }

    if (item.password != item.passwordRetyped) {
        errors.push('Salasõnad ei kattu!')
    }

    if (!validateEmail(item.email)) {
        errors.push('Viga e-maili aadressis!')
    }

    if (!item.phone) {
        errors.push('Sisesta telefoninumber!')
    }

    return errors;

}

function validateUsername(item) {
    let errors = [];

    if (!validateEmail(item.email)) {
        errors.push('Viga e-maili aadressis!');
    }

    if (item.password.length < 4) {
        errors.push('Salasõna minimaalne pikkus on 6 tähemärki!');
    }
}

/* 
    --------------------------------------------
    ALERT FUNCTIONS
    --------------------------------------------
*/

function renderAlert(errors) {
    let alertMessageDiv = document.querySelector('#alertTemplate').content;
    document.querySelector('#form-register').prepend(alertMessageDiv.cloneNode(true));
    document.querySelector('#alertMessage span').textContent = errors;
}

function renderLoginAlert(errors) {
    closeAllAlerts();
    let alertMessageDiv = document.querySelector('#alertTemplate').content;
    document.querySelector('#form-signin').prepend(alertMessageDiv.cloneNode(true));
    document.querySelector('#alertMessage span').textContent = errors;
}

function alertClose() {
    let thisAlert = document.querySelector('.alert');
    thisAlert.remove();
}

function renderSuccess(message) {
    let successMessageDiv = document.querySelector('#successTemplate').content;
    document.querySelector('#form-register').prepend(successMessageDiv.cloneNode(true));
    document.querySelector('#successMessage span').textContent = message;
}

function closeAllAlerts() {
    let currentAlerts = document.querySelectorAll('.alert');
    currentAlerts.forEach(alert => {
        alert.remove();
    });
}

function renderSuccessAlert(message) {
    let successMessageDiv = document.querySelector('#successTemplate').content;
    document.querySelector('#mainContainer').prepend(successMessageDiv.cloneNode(true));
    document.querySelector('#successMessage span').textContent = message;
}